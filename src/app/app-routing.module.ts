import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { PersonalComponent } from './pages/personal/personal.component';
import { ChangeRequestComponent } from './pages/change-request/change-request.component';
import { ChangeRequestinfoComponent } from './pages/change-requestinfo/change-requestinfo.component';

const routes: Routes = [
  { path: '', redirectTo: '/personal', pathMatch: 'full' },
  {path: 'personal' , component: PersonalComponent},
  {path: 'changerequest' , component: ChangeRequestComponent},
  {path: 'changerequestinfo' , component: ChangeRequestinfoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
