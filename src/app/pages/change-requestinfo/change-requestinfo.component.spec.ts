import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeRequestinfoComponent } from './change-requestinfo.component';

describe('ChangeRequestinfoComponent', () => {
  let component: ChangeRequestinfoComponent;
  let fixture: ComponentFixture<ChangeRequestinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeRequestinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeRequestinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
