import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-change-requestinfo',
  templateUrl: './change-requestinfo.component.html',
  styleUrls: ['./change-requestinfo.component.css']
})
export class ChangeRequestinfoComponent implements OnInit {

  changeRequestModel;

  constructor(private activatedroute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedroute.queryParamMap.subscribe(params => {
      this.changeRequestModel = JSON.parse(params.get('changeRequestModel'));
      
    });
  }

}
