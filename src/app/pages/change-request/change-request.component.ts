import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-request',
  templateUrl: './change-request.component.html',
  styleUrls: ['./change-request.component.css']
})
export class ChangeRequestComponent implements OnInit {
  changeRequestModel = {
    option1: false,
    option2: false,
    option3: false,
    option4: false,
    option5: false,
    option6: false,
    option6_1: false,
  };

  constructor(private router: Router) { }

  ngOnInit() {
  }

  checkOtherChange() {
    if (this.changeRequestModel.option6 === false){
      this.changeRequestModel.option6_1 = false;
    }
  }

  onSavedata() {
    this.router.navigate(['/changerequestinfo'], {
      queryParams: {
        changeRequestModel: JSON.stringify(this.changeRequestModel)
      }
    });
  }

}
